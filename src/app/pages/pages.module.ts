import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: 'chat',
  loadChildren: () => import('./chat/chat.module').then(m => m.ChatModule)
}, {
  path: 'contacts',
  loadChildren: () => import('./contacts/contacts.module').then(m => m.ContactsModule)
}, {
  path: 'email',
  loadChildren: () => import('./email/email.module').then(m => m.EmailModule)
}, {
  path: 'search-result',
  loadChildren: () => import('./search-result/search-result.module').then(m => m.SearchResultModule)
}, {
  path: 'users',
  loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
}, {
  path: 'file-manager',
  loadChildren: () => import('./file-manager/file-manager.module').then(m => m.FileManagerModule)
}];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class PagesModule { }
