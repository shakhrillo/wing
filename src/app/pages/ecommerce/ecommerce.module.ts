import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EcommerceComponent } from './ecommerce.component';



@NgModule({
  declarations: [
    EcommerceComponent
  ],
  imports: [
    CommonModule
  ]
})
export class EcommerceModule { }
