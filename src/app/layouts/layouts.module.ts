import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxedLayoutComponent } from './boxed-layout/boxed-layout.component';
import { DefaultLayoutComponent } from './default-layout/default-layout.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    BoxedLayoutComponent,
    DefaultLayoutComponent,
  ],
  imports: [
    RouterModule,
    CommonModule
  ],
  exports: [BoxedLayoutComponent]
})
export class LayoutsModule { }
