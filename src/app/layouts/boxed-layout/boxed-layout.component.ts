import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-boxed-layout',
  templateUrl: './boxed-layout.component.html',
  styleUrls: ['./boxed-layout.component.css']
})
export class BoxedLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
